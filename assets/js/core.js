$(document).ready(function () {
    $(function () {
        $('a[href*=#]:not([href=#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 100
                    }, 1000);
                    return false;
                }
            }
        });
    });
    $(window).scroll(function () {
        var winTop = $(window).scrollTop();
        var offestfirst = $('.header-section').height();
        if (winTop >= offestfirst) {
            $("header.module").addClass("fixed");
        } else {
            $("header.module").removeClass("fixed");
        } //if-else
    }); //win func.
    $('.close-btn').click(function () {
        $('.modul').removeClass('active');
    });
    $('.module-open').click(function (e) {
        e.preventDefault();
        $('.modul').addClass('active');
        $('.main-wrapper').css('overflow-y', 'hidden')
    });
    var count_particles, stats, update;
    stats = new Stats;
    stats.setMode(0);
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';
    document.body.appendChild(stats.domElement);
    //  count_particles = document.querySelector('.js-count-particles');
    update = function () {
        stats.begin();
        stats.end();
        requestAnimationFrame(update);
    };
    requestAnimationFrame(update);
    new WOW().init();
});