    function initMap() {

        var map = new google.maps.Map(document.getElementById('cd-google-map'), {
            center: {
                lat: 44.7789685,
                lng: 10.3435627
            },
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        });
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        directionsDisplay.setMap(map);
        directionsDisplay.setOptions({
            suppressMarkers: true,
            suppressInfoWindows: true
        });

        var latitude = 44.7789685,
            longitude = 10.3435627;
        var marker_url = 'assets/img/cd-icon-location.svg';
        var input = /** @type {!HTMLInputElement} */ (
            document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            map: map,
            visible: true,
            icon: marker_url,
        });
        var newmarker = new google.maps.Marker({
            map: map,
            visible: true,
            icon: marker_url,
        });


        //            Autocomplete 

        autocomplete.addListener('place_changed', function () {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            newmarker.setIcon(({
                url: marker_url
            }));
            newmarker.setPosition(place.geometry.location);
            newmarker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
            }
            start = place.geometry.location;
            var request = {
                origin: start,
                destination: {
                    lat: 44.7789685,
                    lng: 10.3435627
                },
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
            };
            directionsService.route(request, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(result);
                    var routes = result.routes;
                    var leg = routes[0].legs;
                    var lenght = leg[0].distance.text;
                    var duration = leg[0].duration.text;
                }
            });

        });
    }